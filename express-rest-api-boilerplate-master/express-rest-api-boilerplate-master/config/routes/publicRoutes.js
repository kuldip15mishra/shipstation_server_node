const publicRoutes = {
  'GET /orders/:page/:pageSize': 'OrderController.getAll',
};

module.exports = publicRoutes;
