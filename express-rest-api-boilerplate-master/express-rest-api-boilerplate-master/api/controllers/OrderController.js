
const API = require('../services/rest.api');


const OrderController = () => {

  const getAll = async (req, res) => {
    try {
      const params ={page:req.query.page ,pageSize :req.query.pageSize}
      const orders = await API.getAllOrders(params);

      return res.status(200).json({ orders });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };


  return {
    getAll,
  };
};

module.exports = OrderController;
